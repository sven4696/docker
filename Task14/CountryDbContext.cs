﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task14.Models;

namespace Task14
{
    public class CountryDbContext : DbContext
    {
        public DbSet<Country> Countrie { get; set; }

        public CountryDbContext(DbContextOptions<CountryDbContext> contextOptions) : base(contextOptions)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(" Server=mssql1, 1433; Initial Catalog= CountryDb; User ID=sa; Password=Your_password123; Integrated Security= False;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(new Country()
            {
                ID = 1,
                Name = "Switzerland",
                Capital = "Zurich"

            });

            modelBuilder.Entity<Country>().HasData(new Country()
            {
                ID = 2,
                Name = "Norway",
                Capital = "Oslo"

            });

            modelBuilder.Entity<Country>().HasData(new Country()
            {
                ID = 3,
                Name = "China",
                Capital = "Beijing"

            });

            modelBuilder.Entity<Country>().HasData(new Country()
            {
                ID = 4,
                Name = "France",
                Capital = "Paris"

            });

            modelBuilder.Entity<Country>().HasData(new Country()
            {
                ID = 5,
                Name = "Sweden",
                Capital = "Stockholm"

            });

        }
    }
}
