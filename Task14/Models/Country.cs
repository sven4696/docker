﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task14.Models
{
    public class Country
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Capital { get; set; }

    }
}
